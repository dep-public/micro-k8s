service:
    my-mongodb.mongodb.svc.cluster.local

To get the root password run:
    export MONGODB_ROOT_PASSWORD=$(kubectl get secret --namespace mongodb my-mongodb -o jsonpath="{.data.mongodb-root-password}" | base64 --decode)
    password: GrRijbo43v


To connect to your database from outside the cluster execute the following commands:

    kubectl port-forward --namespace mongodb svc/my-mongodb 27017:27017 &
    mongo --host 127.0.0.1 --authenticationDatabase admin -p $MONGODB_ROOT_PASSWORD
