- [1. Deploy Juju With Micro K8s](#1-deploy-juju-with-micro-k8s)
  - [1.1. <u>Installing MicroK8s</u>](#11-uinstalling-microk8su)
  - [1.2. <u>Use private registry</u>](#12-uuse-private-registryu)
  - [1.3. <u>Applications</u>](#13-uapplicationsu)
    - [1.3.1. <u>Helm Repo</u>](#131-uhelm-repou)
    - [1.3.2. <u>MongoDB</u>](#132-umongodbu)
    - [1.3.3. <u>Rabbit MQ</u>](#133-urabbit-mqu)
    - [1.3.4. <u>MySql</u>](#134-umysqlu)
    - [1.3.5. <u>Grafana</u>](#135-ugrafanau)
  - [1.4. <u>Install Juju</u>](#14-uinstall-jujuu)

# 1. Deploy Juju With Micro K8s
___


## 1.1. <u>Installing MicroK8s</u>

Install Microk8s

```bash
sudo snap install microk8s --classic --channel=1.23
```

Config User Group

```bash
sudo usermod -a -G microk8s $USER
sudo chown -f -R $USER ~/.kube
```

Alias for kubectl

```bash
echo "alias kubectl='microk8s kubectl'" >  ~/.bash_aliases
echo "alias helm='microk8s helm3'" >>  ~/.bash_aliases
```

Re-login

Test

```bash
microk8s kubectl get nodes
kubectl get nodes
```

Check and Enable Add-ons

```bash
microk8s status
microk8s enable dashboard
microk8s enable dns
microk8s enable helm3
microk8s enable ingress
microk8s enable metrics-server
microk8s enable storage
```

When necessary Stop/Start

```bash
microk8s stop
microk8s start
```

## 1.2. <u>Use private registry</u>

```bash
# Copy Registry CA
cp ca.crt /etc/ssl/certs
sudo update-ca-certificates
# Create Docker Secret
microk8s.kubectl create secret docker-registry regcred11 \
--docker-server=registry.gitlab.home.com:8443 \
--docker-username=user \
--docker-password=userpass \
--docker-email=oi@oi.com

# Use it with the POD
  containers:
  - name: private-reg-container
    image: registry.gitlab.home.com:8443/a/b/pyapp:1.2
  imagePullSecrets:
  - name: regcred11
```


## 1.3. <u>Applications</u>

### 1.3.1. <u>Helm Repo</u>
```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
```

### 1.3.2. <u>MongoDB</u>

```bash
kubectl create ns mongodb
helm install my-mongodb bitnami/mongodb -n=mongodb
kubectl patch service/my-mongodb -n=mongodb -p '{"spec": {"type": "NodePort"}}'
kubectl patch service/my-mongodb -n=mongodb --patch \
  '{"spec": { "type": "NodePort", "ports": [ { "nodePort": 32000, "port": 27017, "protocol": "TCP", "targetPort": "mongodb" } ] } }' \
  --type=merge
```


### 1.3.3. <u>Rabbit MQ</u>

```bash
kubectl create ns rabbitmq
helm repo add bitnami https://charts.bitnami.com/bitnami
helm install my-rabbitmq -n=rabbitmq bitnami/rabbitmq
kubectl patch svc my-rabbitmq -n=rabbitmq -p '{"spec": {"type": "NodePort"}}'
kubectl patch svc my-rabbitmq -n=rabbitmq --patch \
  '{"spec": { "type": "NodePort", "ports": [ 
    { "name": "amqp", "nodePort": 32001, "port": 5672, "protocol": "TCP", "targetPort": "amqp" }, 
    { "name": "epmd", "nodePort": 32002, "port": 4369, "protocol": "TCP", "targetPort": "epmd" }, 
    { "name": "dist", "nodePort": 32003, "port": 25672, "protocol": "TCP", "targetPort": "dist" }, 
    { "name": "http-stats", "nodePort": 32004, "port": 15672, "protocol": "TCP", "targetPort": "amqstatsp" }
    ] } }' \
  --type=merge
```


### 1.3.4. <u>MySql</u>

```bash
kubectl create ns mysql
helm install my-mysql -n=mysql bitnami/mysql
kubectl patch svc my-mysql -n=mysql -p '{"spec": {"type": "NodePort"}}'
kubectl patch svc my-mysql -n=mysql --patch \
  '{"spec": { "type": "NodePort", "ports": [ 
    { "name": "mysql", "nodePort": 32005, "port": 3306, "protocol": "TCP", "targetPort": "mysql" }
    ] } }' \
  --type=merge
```


### 1.3.5. <u>Grafana</u>

```bash
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update
kubectl create ns grafana
helm install my-grafana -n=grafana bitnami/grafana
kubectl patch svc my-grafana -n=grafana -p '{"spec": {"type": "NodePort"}}'
kubectl patch svc my-grafana -n=grafana --patch \
  '{"spec": { "type": "NodePort", "ports": [ 
    { "name": "service", "nodePort": 32006, "port": 80, "protocol": "TCP", "targetPort": 3000 }
    ] } }' \
  --type=merge
```




## 1.4. <u>Install Juju</u>

Install Juju

```bash
sudo snap install --classic juju
```

Check Clouds

```bash
juju clouds
```

Bootstrap

```bash
juju bootstrap microk8s my-k8s
```

Add a model (Equivalente to namespace)

```bash
juju add-model core
```

Deploy WorkLoads

```bash
juju deploy mattermost-k8s
juju deploy postgresql-k8s
```

Create a Relation
```bash
juju relate mattermost-k8s postgresql-k8s:db
```